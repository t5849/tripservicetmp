package tripService.services.serviceImpl;

import org.springframework.stereotype.Service;
import tripService.models.trip.Invite;
import tripService.models.trip.Trip;
import tripService.repositories.postgres.InviteRepository;
import tripService.services.InviteService;

import java.util.UUID;

@Service
public class InviteServiceImpl implements InviteService {
    private final InviteRepository repo;

    public InviteServiceImpl(InviteRepository repo) {
        this.repo = repo;
    }

    @Override
    public Invite addInvite(Invite invite) {
        return repo.save(invite);
    }

    @Override
    public Invite getInviteByTrip(Trip trip) {
        return repo.getInviteByTrip(trip);
    }

    @Override
    public Invite revokeInvite(Trip trip) {
        repo.deleteByTrip(trip);
        return repo.save(new Invite(trip));
    }

    @Override
    public Invite getInviteById(UUID inviteId) {
        return repo.findById(inviteId).orElse(null);
    }
}
