package tripService.services.serviceImpl;

import com.google.common.collect.Lists;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tripService.models.trip.Event;
import tripService.models.trip.FileInfo;
import tripService.models.trip.Invite;
import tripService.models.trip.Trip;
import tripService.repositories.postgres.EventRepository;
import tripService.repositories.postgres.InviteRepository;
import tripService.repositories.postgres.TripRepository;
import tripService.services.InviteService;
import tripService.services.TripService;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("unused")
@Service
public class TripServiceImpl implements TripService {
    private final TripRepository repo;
    private final InviteService inviteService;
    private final InviteRepository inviteRepository;
    private final EventRepository eventRepository;

    @Autowired
    public TripServiceImpl(TripRepository repo, InviteServiceImpl inviteService, InviteRepository inviteRepository, EventRepository eventRepository) {
        this.repo = repo;
        this.inviteService = inviteService;
        this.inviteRepository = inviteRepository;
        this.eventRepository = eventRepository;
    }

    @Override
    public Trip addTrip(Trip trip) {
        var savedTrip = repo.save(trip);
        Invite invite = inviteService.addInvite(new Invite(savedTrip));
        return savedTrip;
    }

    @Override
    public Trip update(Trip trip) {
        var events = trip.getEvents().stream();
        var updatedTrip = repo.save(trip);
        for (Event event : events.collect(Collectors.toList())) {
            if (event.getTrip() == null) {
                event.setTrip(updatedTrip);
            }
//            if (event.getEventId() == null)
//            {
            this.eventRepository.save(event);
//            }
        }
        return updatedTrip;
    }

    @Override
    public Trip getTripById(long id) {
        return repo.findById(id).orElse(null);
    }

    @Override
    public FileInfo[] getFilesById(long id) {
        var trip = repo.findById(id).orElse(null);
        if (trip != null) {
            return trip.
                    getEvents().
                    stream().map(x ->
                            x.getFileIds().
                                    toArray(new FileInfo[0])
                    ).
                    flatMap(Stream::of).
                    toArray(FileInfo[]::new);
        }
        return new FileInfo[0];
    }

    @Override
    public Trip getTripByInviteId(UUID inviteId) {
        var invite = inviteRepository.findById(inviteId).orElse(null);
        return invite == null ? null : invite.getTrip();
    }

    @Override
    public List<Trip> getAllTrips() {
        return Lists.newArrayList(repo.findAll());
    }

    @Override
    public void deleteById(long id) {
        repo.findById(id).ifPresent(repo::delete);
    }

    @Override
    public Trip deleteEvent(Trip trip, long eventId) {
        trip.setEvents(trip.getEvents().stream().
                filter(event -> event.getEventId() != eventId).collect(Collectors.toList()));
        eventRepository.deleteById(eventId);
        return repo.save(trip);
    }

    public List<Long> getParticipants(Long tripId) {
        return repo.findById(tripId).get().getParticipantsId();
    }


    @Override
    public List<Long> getGoalsIds(Long tripId) {
        //todo if trip is null
        return eventRepository.findEventsByTrip(repo.findById(tripId).get())
                .stream()
                .map(Event::getGoalId)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
//        return null;
    }

    @Override
    public Long getOwnerId(Long tripId) {
        if (repo.findById(tripId).isPresent()) {
            return repo.findById(tripId).get().getOwnerId();
        } else {
            return null;
        }
    }
}
