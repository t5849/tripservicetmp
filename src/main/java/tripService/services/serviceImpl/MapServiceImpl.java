package tripService.services.serviceImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.SneakyThrows;
import lombok.var;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tripService.exception.SubjectNotFoundEx;
import tripService.models.map.*;
import tripService.repositories.mongo.MapObjectRepository;
import tripService.repositories.postgres.SubjectEnRepository;
import tripService.repositories.postgres.SubjectRepository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class MapServiceImpl {
    @Value("${YANDEXKEY:}")
    private String YANDEX_KEY;

    @Value("${URL_TO_YANDEX:}")
    private String URL_T0_YANDEX;

    private final MapObjectRepository mapObjectRepository;
    private final SubjectRepository subjectRepository;
    private final SubjectEnRepository subjectEnRepository;

    private final List<ObjectType> DEFAULT_TYPES = Arrays.asList(
            new ObjectType("monument", "pink"),
            new ObjectType("hotels", "green"),
            new ObjectType("institution", "blue"),
            new ObjectType("bars", ""),
            new ObjectType("restaurants", ""),
            new ObjectType("cinemas", "black"),
            new ObjectType("square", ""),
            new ObjectType("viewpoint", ""),
            new ObjectType("zoo", ""),
            new ObjectType("theatre", ""),
            new ObjectType("сafe", "white"),
            new ObjectType("malls", "yellow"),
            new ObjectType("attraction", "red"),
            new ObjectType("park", "black"),
            new ObjectType("concert hall", "orange"),
            new ObjectType("stadium", "purple"),
            new ObjectType("museum", "gray"));

    public MapServiceImpl(MapObjectRepository repo, SubjectRepository subjectRepository, SubjectEnRepository subjectEnRepository) {
        this.mapObjectRepository = repo;
        this.subjectRepository = subjectRepository;
        this.subjectEnRepository = subjectEnRepository;
    }

    public MapObject save(MapObject object) {
        return mapObjectRepository.save(object);
    }

    public void deleteById(String id) {
        mapObjectRepository.deleteById(id);
    }

    public MapObject getById(String id) {
        return mapObjectRepository.findById(id).orElse(null);
    }

    public void delete(MapObject object) {
        mapObjectRepository.delete(object);
    }

    public Stream<Subject> getSubjects()
    {
        return StreamSupport.
                stream(subjectRepository.findAll().spliterator(), false).
                sorted(Comparator.comparingInt(Subject::getId));
    }

    public Stream<SubjectEn> getSubjectsEn()
    {
        System.out.println(URL_T0_YANDEX);
        return StreamSupport.
                stream(subjectEnRepository.findAll().spliterator(), false).
                sorted(Comparator.comparingInt(SubjectEn::getId));
    }

    // lon and lat - координаты центра города
    @SneakyThrows
    public void saveMapObjects(double centerLon, double centerLat) throws JsonProcessingException {
        for (ObjectType type : DEFAULT_TYPES) {
            ResponseEntity<String> response = makePostToYandex(type.getTypeName(), centerLon, centerLat);

            try {
                List<MapObject> result = parseDataAndSaveToDB(response);
                mapObjectRepository.saveAll(result);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            Thread.sleep(1000);
//            mapObjectRepository.saveAll(result);
        }
//        return mapObjectRepository.findAll();
    }




    //todo delete hardcoded ll values (use params from GET)
    private ResponseEntity<String> makePostToYandex(String category, double lon, double lat) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        var url = String.format(URL_T0_YANDEX,
                YANDEX_KEY,
                category,
                lon + ", " + lat); // 39.212 + ", " + 51.674
        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }

    public List<MapObject> getMapObjects(double lon, double lat) {
        List<MapObject> poisByLon = mapObjectRepository.findByLonBetween(lon - 1, lon + 1);
        List<MapObject> poisByLat = mapObjectRepository.findByLatBetween(lat - 1, lat + 1);
        return mergeLists(poisByLon, poisByLat);
    }

    public boolean isPlacesExist(double lon, double lat) {
        return getMapObjects(lon, lat).size() > 200;
    }

    public List<MapObject> getMapObjectByType(double lon, double lat, String type) {
        if (isPlacesExist(lon, lat)) {
            return getMapObjects(lon, lat).stream().filter(x -> x.getTypes().contains(type)).collect(Collectors.toList());
        } else {
            try {
                saveMapObjects(lon, lat);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return getMapObjects(lon, lat).stream().filter(x -> x.getTypes().contains(type)).collect(Collectors.toList());
        }
//        return null;
    }

//    public boolean isCorrectType(MapObject obj, String type) {
//        obj.getTypes().stream().filter(x -> x.equals(type))
//    }

    public List<MapObject> getMapObjects(int id) {
        Subject city = subjectRepository.findById(id).orElse(null);
        if (city != null) {
            double lon = city.getLon();
            double lat = city.getLat();
            List<MapObject> poisByLon = mapObjectRepository.findByLonBetween(lon - 0.5, lon + 0.5);
            List<MapObject> poisByLat = mapObjectRepository.findByLatBetween(lat - 0.5, lat + 0.5);
            return mergeLists(poisByLon, poisByLat);
        } else {
            throw new SubjectNotFoundEx();
        }
    }

    private List<MapObject> parseDataAndSaveToDB(ResponseEntity<String> response) throws JsonProcessingException {
        List<MapObject> objects = new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        ObjectNode root = new ObjectMapper().readValue(response.getBody(), ObjectNode.class);
        JsonNode features = root.get("features");
        String objectsAsString = features.toString();
        ObjectNode[] objectsAsArray = objectMapper.readValue(objectsAsString, ObjectNode[].class);

        for (ObjectNode node : objectsAsArray) {
            var tmp = node.get("geometry").get("coordinates").toString();
            Center center = new Center(node.get("geometry").get("coordinates").toString().substring(1, tmp.length() - 1).split(","));
            var coords = node.get("geometry").get("coordinates").toString().substring(1, tmp.length() - 1).split(",");

            String lon = coords[0];
            String lat = coords[1];

            String name = node.get("properties").get("name").asText();
            String address = node.get("properties").get("description").asText();

            JsonNode categories = node.get("properties").get("CompanyMetaData").get("Categories");
            ObjectNode[] typesNode = objectMapper.readValue(categories.toString(), ObjectNode[].class);
            List<String> types = new ArrayList<>();

            for (ObjectNode type : typesNode) {
                try {
                    types.add(type.get("class").asText());
                } catch (NullPointerException ex) {
                    System.out.println(type.asText());
                }
//                types.add(type.get("class").asText());
            }

            objects.add(new MapObject(name, address, Double.parseDouble(lon), Double.parseDouble(lat), types));
        }
        return objects;
    }

    private List<MapObject> mergeLists(List<MapObject> byLon, List<MapObject> byLat) {
        List<MapObject> result = new ArrayList<>();
        for (MapObject obj : byLon) {
            for (MapObject obj2 : byLat) {
                if (obj.equals(obj2)) {
                    result.add(obj);
                }
            }
        }
        return result;
    }

    public List<ObjectType> getTypes() {
        return DEFAULT_TYPES;
    }

}
