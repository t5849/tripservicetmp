package tripService.services;

import tripService.models.trip.Invite;
import tripService.models.trip.Trip;

import java.util.UUID;

public interface InviteService {
    Invite addInvite(Invite invite);

    Invite getInviteByTrip(Trip trip);

    Invite revokeInvite(Trip trip);

    Invite getInviteById(UUID inviteId);
}
