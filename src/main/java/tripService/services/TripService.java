package tripService.services;

import tripService.models.trip.FileInfo;
import tripService.models.trip.Trip;

import java.util.List;
import java.util.UUID;

public interface TripService {
    Trip addTrip(Trip trip);
    Trip update(Trip trip);
    Trip getTripById(long id);
    Trip getTripByInviteId(UUID inviteId);
    List<Trip> getAllTrips();
    FileInfo[] getFilesById(long id);
    void deleteById(long id);

    Trip deleteEvent(Trip trip, long eventId);

    List<Long> getParticipants(Long tripId);

    List<Long> getGoalsIds(Long tripId);

    Long getOwnerId(Long tripId);
}
