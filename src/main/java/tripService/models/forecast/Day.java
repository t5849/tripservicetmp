package tripService.models.forecast;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Day {
    private Temperature actualTemp;
    private Temperature feelsLikeTemp;
    private Date date;
    private Date sunrise;
    private Date sunset;
    private String description;
    private Long id;
}
