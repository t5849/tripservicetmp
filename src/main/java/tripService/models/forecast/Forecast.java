package tripService.models.forecast;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Forecast {
    private List<Day> week = new ArrayList<>();
}
