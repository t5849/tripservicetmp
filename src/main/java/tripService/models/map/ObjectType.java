package tripService.models.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ObjectType {
    private String typeName;
    private String typeColor;
}
