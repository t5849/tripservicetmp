package tripService.models.map;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class MapObject {

    @Id
    private String id;

    private String name;
    private String address;
    private double lon;
    private double lat;
    private List<String> types;

    public MapObject(String name, String address, double lon, double lat, List<String> types) {
        this.name = name;
        this.address = address;
        this.lon = lon;
        this.lat = lat;
        this.types = types;
    }
}