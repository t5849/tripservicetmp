package tripService.models.map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class Center {
    private String latitude;
    private String longitude;

    public Center(String[] coordinates) {
        latitude = coordinates[0];
        longitude = coordinates[1];
    }
}
