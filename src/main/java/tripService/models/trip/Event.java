package tripService.models.trip;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

//@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private Long eventId;

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "trip_id", nullable = false)
    private Trip trip;

    @Column(name = "description", nullable = false)
    private String desc;

    @Column(nullable = false)
    private Double latitude;

    @Column(nullable = false)
    private Double longitude;

    //    @Column(name = "goalId")
    @ElementCollection
    @CollectionTable(
            name = "goals",
            joinColumns = @JoinColumn(name = "event_id")
    )
    private List<Long> goalId;

    //todo
    @Column(nullable = false, columnDefinition = "varchar(255) default ''")
    private String creator;

    //todo
    @Column(nullable = false, columnDefinition = "varchar(255) default ''")
    private String duration;

    //todo
    private String fileId;

    @Column(columnDefinition = "integer default 0")
    private Integer day;

    @Column(name = "day_order", columnDefinition = "integer default 0")
    private Integer order;

    @ElementCollection
    @CollectionTable(
            name = "file_info",
            joinColumns = @JoinColumn(name = "event_id")
    )
    private List<FileInfo> fileIds;
}
