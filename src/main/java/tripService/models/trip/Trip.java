package tripService.models.trip;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "trip")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trip_id")
    private Long tripId;

    @Column(nullable = false)
    private String name;

    @Column(name = "ownerId", nullable = false, updatable = false)
    private Long ownerId;

    @Column(nullable = false)
    private Double latitude;

    @Column(nullable = false)
    private Double longitude;

    @ElementCollection
    @CollectionTable(
            name = "userTrip",
            joinColumns = @JoinColumn(name = "trip_id")
    )
    private List<Long> participantsId;

//    @ElementCollection
//    @CollectionTable(
//            name = "event",
//            joinColumns = @JoinColumn(name = "trip_id")
//    )
    @OneToMany(mappedBy = "trip")
    private List<Event> events;
}
