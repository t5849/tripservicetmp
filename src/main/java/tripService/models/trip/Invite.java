package tripService.models.trip;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "invite")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Invite {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "inviteId")
    private UUID inviteId;

    @OneToOne()
    @JoinColumn(name = "trip_id"/*, referencedColumnName = "trip_id"*/)
    @JsonBackReference
    private Trip trip;

    public Invite(Trip trip) {
        this.trip = trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }
}
