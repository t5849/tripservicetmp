package tripService.repositories.postgres;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tripService.models.map.Subject;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Integer> {
    Subject findDistinctByLatAndLon(double lat, double lon);
}
