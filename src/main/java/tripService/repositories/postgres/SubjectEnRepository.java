package tripService.repositories.postgres;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tripService.models.map.Subject;
import tripService.models.map.SubjectEn;

@Repository
public interface SubjectEnRepository extends CrudRepository<SubjectEn, Integer> {
    Subject findDistinctByLatAndLon(double lat, double lon);
}
