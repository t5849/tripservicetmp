package tripService.repositories.postgres;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tripService.models.trip.Invite;
import tripService.models.trip.Trip;

import java.util.UUID;

@Repository
public interface InviteRepository extends CrudRepository<Invite, UUID> {

    void deleteByTrip(Trip trip);

    Invite getInviteByTrip(Trip trip);
}
