package tripService.repositories.postgres;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tripService.models.trip.Event;
import tripService.models.trip.Trip;

import java.util.List;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

    List<Event> findEventsByTrip(Trip trip);
}
