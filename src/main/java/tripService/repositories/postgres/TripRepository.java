package tripService.repositories.postgres;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tripService.models.trip.Trip;

import java.util.List;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {

//    List<Long> get(Long tripId);
}
