package tripService.repositories.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import tripService.models.map.MapObject;

import java.util.List;

@Repository
public interface MapObjectRepository extends MongoRepository<MapObject, String> {

    List<MapObject> findByName(String name);

    List<MapObject> findByLonBetween(double up, double down);

    List<MapObject> findByLatBetween(double up, double down);
}
