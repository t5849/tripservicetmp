package tripService.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tripService.models.map.MapObject;
import tripService.models.map.ObjectType;
import tripService.models.map.Subject;
import tripService.models.map.SubjectEn;
import tripService.services.serviceImpl.MapServiceImpl;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/maps")
public class MapController {

    private final MapServiceImpl mapService;

    @Autowired
    private Environment environment;

    @GetMapping()
    public String[] getgovno() {
        return environment.getActiveProfiles();
    }

    public MapController(MapServiceImpl mapService) {
        this.mapService = mapService;
    }

    @GetMapping(value = "/subjects")
    public ResponseEntity<List<SubjectEn>> getSubjects(
            @RequestParam("start") int start,
            @RequestParam("count") int count,
            @RequestParam("search") String search
            ) {
        return new ResponseEntity<>(this.mapService.
                getSubjectsEn().
                filter(x -> x.getName().toLowerCase(Locale.ROOT).contains(search.toLowerCase(Locale.ROOT))).
                skip(start).
                limit(count).
                collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @GetMapping(value = "/subjectsRu")
    public ResponseEntity<List<Subject>> getSubjectsRu(
            @RequestParam("start") int start,
            @RequestParam("count") int count,
            @RequestParam("search") String search
    ) {
        //test comment
        return new ResponseEntity<>(this.mapService.
                getSubjects().
                filter(x -> x.getName().toLowerCase(Locale.ROOT).contains(search.toLowerCase(Locale.ROOT))).
                skip(start).
                limit(count).
                collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @GetMapping("/mapObjects")
    public ResponseEntity<List<MapObject>> getObjects(/*@RequestParam("id") int id*/@RequestParam("lon") double lon,
                                                                                    @RequestParam("lat") double lat,
                                                                                    @RequestParam("type") String type) {
        return new ResponseEntity<>(mapService.getMapObjectByType(lon ,lat, type), HttpStatus.OK);
    }


    @GetMapping(value = "/types")
    public ResponseEntity<List<ObjectType>> getObjectTypes() {
        return new ResponseEntity<>(mapService.getTypes(), HttpStatus.OK);
    }

//    @GetMapping("/test")
//    public ResponseEntity<List<MapObject>> getObjects2() throws JsonProcessingException {
//        return new ResponseEntity<>(mapService.initialSaveMapObjects(40, 45), HttpStatus.OK);
//    }
}
