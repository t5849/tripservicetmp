package tripService.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.var;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import tripService.models.forecast.Day;
import tripService.models.forecast.Forecast;
import tripService.models.forecast.Temperature;

import java.util.Date;

@RestController
@RequestMapping("/weather")
public class WeatherController {
    @Value("${api_key_owm:}")
    private String api_key;

    @Value("${forecast_url:}")
    private String forecast_url;

    @GetMapping
    public ResponseEntity<Forecast> getForecast(@RequestParam String lat, @RequestParam String lon) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        var url = String.format(forecast_url, lat, lon, api_key);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        return new ResponseEntity<>(buildForecastObj(response.getBody()), HttpStatus.OK);
    }

    private Forecast buildForecastObj(String jsonResponse) throws JsonProcessingException {
        Forecast forecast = new Forecast();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        ObjectNode root = new ObjectMapper().readValue(jsonResponse, ObjectNode.class);
        JsonNode daily = root.get("daily");
        ObjectNode[] days = objectMapper.readValue(daily.toString(), ObjectNode[].class);

        // дата в миллисекундах
        for (ObjectNode dayNode : days) {
            Date date = new Date(dayNode.get("dt").asLong() * 1000);
            Date sunrise = new Date(dayNode.get("sunrise").asLong() * 1000);
            Date sunset = new Date(dayNode.get("sunset").asLong() * 1000);

            var temp = dayNode.get("temp");
            Temperature actualTemp = new Temperature();
            actualTemp.setMorning((int) Math.round(temp.get("morn").asDouble()));
            actualTemp.setDay((int) Math.round(temp.get("day").asDouble()));
            actualTemp.setEvening((int) Math.round(temp.get("eve").asDouble()));
            actualTemp.setNight((int) Math.round(temp.get("night").asDouble()));

            temp = dayNode.get("feels_like");
            Temperature feelsLikeTemp = new Temperature();
            feelsLikeTemp.setMorning((int) Math.round(temp.get("morn").asDouble()));
            feelsLikeTemp.setDay((int) Math.round(temp.get("day").asDouble()));
            feelsLikeTemp.setEvening((int) Math.round(temp.get("eve").asDouble()));
            feelsLikeTemp.setNight((int) Math.round(temp.get("night").asDouble()));

            ObjectNode[] tmp = objectMapper.readValue(dayNode.get("weather").toString(), ObjectNode[].class);
            String description = tmp[0].get("description").asText();
            Long id = tmp[0].get("id").asLong();

            Day day = new Day(actualTemp, feelsLikeTemp, date, sunrise, sunset, description, id);
            forecast.getWeek().add(day);
        }
        return forecast;
    }

    @GetMapping("/key")
    public String getApi_key() {
        return api_key;
    }
}
