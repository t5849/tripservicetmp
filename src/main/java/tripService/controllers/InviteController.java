package tripService.controllers;

import lombok.var;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tripService.exception.TripNotFoundEx;
import tripService.models.trip.Invite;
import tripService.services.InviteService;
import tripService.services.TripService;

import java.util.Arrays;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/invite")
public class InviteController {
    private final InviteService inviteService;
    private final TripService tripService;

    public InviteController(InviteService inviteService, TripService tripService) {
        this.inviteService = inviteService;
        this.tripService = tripService;
    }

    @GetMapping
    public ResponseEntity<Invite> getInviteByTrip(@RequestParam("tripId") Long tripId) {
        var trip = tripService.getTripById(tripId);
        if (trip == null) {
            throw new TripNotFoundEx();
        } else {
            Invite invite = inviteService.getInviteByTrip(trip);
            return new ResponseEntity<>(invite, HttpStatus.OK);
        }
    }

    @GetMapping("/getInvitesByTripIds")
    public Invite[] getUserNamesByUserIds(@RequestParam Long[] tripId) {
        return Arrays.
                stream(tripId).
                map(tripService::getTripById).
                map(inviteService::getInviteByTrip).
                toArray(Invite[]::new);
    }

    @GetMapping("/revoke")
    @Transactional
    public ResponseEntity<Invite> revokeInvite(@RequestParam("tripId") Long tripId) {
        var trip = tripService.getTripById(tripId);
        if (trip == null) {
            throw new TripNotFoundEx();
        } else {
            try
            {
                Invite invite = inviteService.revokeInvite(trip);
                return new ResponseEntity<>(invite, HttpStatus.OK);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                throw ex;
            }
        }
    }
}
