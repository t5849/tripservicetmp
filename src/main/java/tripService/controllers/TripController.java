package tripService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import tripService.exception.InviteNotFoundEx;
import tripService.exception.TripNotFoundEx;
import tripService.exception.UserNotFoundEx;
import tripService.models.trip.Invite;
import tripService.models.trip.Trip;
import tripService.services.InviteService;
import tripService.services.TripService;

import java.util.List;
import java.util.UUID;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/trips")
public class TripController {

    private static final String URI_GET_USER = "http://usrsrvinstance:8084/test";
    private static final String URI_VALIDATE = "http://auth_instance:8082/tests/validate2/{token}";

    private final TripService tripService;
    private final InviteService inviteService;

    @Autowired
    public TripController(TripService tripService, InviteService inviteService) {
        this.tripService = tripService;
        this.inviteService = inviteService;
    }

    @PostMapping
    public ResponseEntity<Trip> create(@RequestBody Trip trip, @RequestHeader("Authorization") String token) {
        Long ownerId = Long.parseLong(isTokenValid(token));
        trip.setOwnerId(ownerId);
        if (isOwnerExist(trip.getOwnerId())) {
            return new ResponseEntity<>(tripService.addTrip(trip), HttpStatus.CREATED);
        }
        else
            throw new UserNotFoundEx();
    }


    @PostMapping("/test")
    public ResponseEntity<Trip> createTest(@RequestBody Trip trip) {
        if (true) {
            return new ResponseEntity<>(tripService.addTrip(trip), HttpStatus.CREATED);
        }
        else
            throw new UserNotFoundEx();
    }

    @GetMapping("/test2")
    public ResponseEntity<Boolean> getTestBool() {
        return new ResponseEntity<>(getTestBoolFromUsers(), HttpStatus.OK);
    }

    private boolean getTestBoolFromUsers()
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Boolean> response = restTemplate.exchange(
                URI_GET_USER,
                HttpMethod.GET,
                entity,
                Boolean.class);
        return response.getBody() != null;
    }

    /**
     * check valid ownerId
     * @param ownerId owner id
     * @return true if owner exist
     */
    private boolean isOwnerExist(Long ownerId)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URI_GET_USER,
                HttpMethod.GET,
                entity,
                String.class,
                ownerId);
        return response.getBody() != null;
    }

    private String isTokenValid(String token)
    {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URI_VALIDATE,
                HttpMethod.GET,
                entity,
                String.class,
                token
                );
        return response.getBody();
    }

    @PutMapping
    public ResponseEntity<Trip> updateTrip(@RequestBody Trip updatedTrip) {
        return ResponseEntity.ok(tripService.update(updatedTrip));
    }

    @DeleteMapping("/event/{id}")
    public ResponseEntity<Trip> deleteEvent(@RequestBody Trip trip, @PathVariable("id") long id) {
        return ResponseEntity.ok(tripService.deleteEvent(trip, id));
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Trip> getTripById(@PathVariable("id") long id) throws UserNotFoundEx {
        Trip trip = tripService.getTripById(id);
        if (trip != null)
            return new ResponseEntity<>(tripService.getTripById(id), HttpStatus.OK);
        else
            throw new TripNotFoundEx();
    }

    @GetMapping("/join")
    public ResponseEntity<Trip> getTripByInvite(@RequestParam("inviteId") UUID inviteId) {
        Invite invite = inviteService.getInviteById(inviteId);
        if (invite == null) {
            throw  new InviteNotFoundEx();
        } else {
            Trip trip = tripService.getTripByInviteId(inviteId);
            if (trip != null)
                return new ResponseEntity<>(trip, HttpStatus.OK);
            else
                throw new InviteNotFoundEx();
        }
    }

    @GetMapping
    public ResponseEntity<List<Trip>> getAllTrips() {
        return new ResponseEntity<>(tripService.getAllTrips(), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteTripById(@RequestParam("id") long id) {
        if (tripService.getTripById(id) != null) {
            tripService.deleteById(id);
            return ResponseEntity.ok("Successfully deleted");
        } else
            throw new TripNotFoundEx();
    }

    @GetMapping("/participants")
    public ResponseEntity<List<Long>> getParticipants(@RequestParam Long tripId) {
        return new ResponseEntity<>(tripService.getParticipants(tripId), HttpStatus.OK);
    }

    @GetMapping("/goalsIds/{tripId}")
    public ResponseEntity<List<Long>> getGoalsIds(@PathVariable("tripId") Long tripId) {
        return new ResponseEntity<>(tripService.getGoalsIds(tripId), HttpStatus.OK);
    }

    @GetMapping("/owner/{tripId}")
    public ResponseEntity<Long> getOwnerId(@PathVariable("tripId") Long tripId) {
        return new ResponseEntity<>(tripService.getOwnerId(tripId), HttpStatus.OK);
    }
}