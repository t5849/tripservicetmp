package tripService.exception;

public class UserNotFoundEx extends RuntimeException {
    public UserNotFoundEx() {
        super("USER NOT FOUND");
    }
}
