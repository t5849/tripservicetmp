package tripService.exception;

public class TripNotFoundEx extends RuntimeException {
    public TripNotFoundEx() {
        super("Trip not found");
    }
}
