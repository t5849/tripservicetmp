package tripService.exception;

public class SubjectNotFoundEx extends RuntimeException {

    public SubjectNotFoundEx() {
        super("Subject not found");
    }
}
