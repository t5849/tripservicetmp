package tripService.exception;

public class InviteNotFoundEx extends RuntimeException {
    public InviteNotFoundEx() {
        super("Invite not found");
    }
}
