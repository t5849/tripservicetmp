package tripService.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
//@SuppressWarnings("unused")
public class ExHandler {
//    private static final Logger log = LoggerFactory.getLogger(ExHandler.class.getName());

    @ExceptionHandler({
            TripNotFoundEx.class, UserNotFoundEx.class, SubjectNotFoundEx.class,
            InviteNotFoundEx.class, LocationNotFoundEx.class})
    public ResponseEntity<Error> handleException(Exception e) {
//        log.error("Exception: ", e);
        return new ResponseEntity<>(new Error(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handle(Exception e) {
//        log.error("Exception: ", e);
        e.printStackTrace();
        return new ResponseEntity<>(new Error("Something went wrong."), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
