package tripService.exception;

public class LocationNotFoundEx extends RuntimeException {
    public LocationNotFoundEx() {
        super("Subject not found");
    }
}
