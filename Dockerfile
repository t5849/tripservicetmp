FROM openjdk:11-jre-slim AS run
COPY target/tripService-latest.jar /usr/local/lib/app.jar
ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]
